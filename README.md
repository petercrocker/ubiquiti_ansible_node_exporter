# Ubiquiti EdgeOS - Install Prometheus node_exporter via ansible

A simple example of using ansible to install and run [Prometheus node_exporter](https://github.com/prometheus/node_exporter) on a Ubiquiti EdgeMax router running EdgeOS 2.0.6.

More detailed blog entry explaining the setup here: 